package main

import (
	"bytes"
	"encoding/pem"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"
	"text/template"

	ctb "gitlab.kit.edu/kit/kit-ca/CertificateToolbox"
)

var (
	FilenameTemplates = map[string]string{
		"name_end":    `{{ .Subject.CommonName }} ({{ .NotAfter.Format "2006-01-02" }})`,
		"name_begin":  `{{ .Subject.CommonName }} ({{ .NotBefore.Format "2006-01-02" }})`,
		"name_serial": `{{ .Subject.CommonName }} - 0x{{ .SerialNumber.Text 16 }}`,
	}
	FilenameSuffix, FilenameTemplateString, ConfigTemplateName string
	FilenameTemplate                                           *template.Template
	configListTemplates, configDryRun                          bool
)

func init() {
	//log.SetFlags(log.LstdFlags | log.Lshortfile)
	flag.BoolVar(&configDryRun, "n", false, "Dry run (don't create files)")
	flag.BoolVar(&configListTemplates, "list-templates", false, "List all predefined template names and exit")
	flag.StringVar(&FilenameTemplateString, "templatestring", FilenameTemplates["name_end"], "Template for generating filenames")
	flag.StringVar(&ConfigTemplateName, "template", "name_end", "Select a filename template (use -list-templates for a listing)")
	flag.StringVar(&FilenameSuffix, "suffix", "pem", "Choose a custom filename suffix (pem, cert, crt, …)")
	flag.Parse()
}

func main() {
	var (
		err error
	)
	if configListTemplates {
		for name, value := range FilenameTemplates {
			fmt.Fprintf(os.Stderr, "  %s : %s\n", name, value)
		}
		os.Exit(0)
	}
	// Look for named template
	_, ok := FilenameTemplates[ConfigTemplateName]
	if !ok {
		log.Fatalf("No template named \"%s\"available", ConfigTemplateName)
	}
	FilenameTemplateString = FilenameTemplates[ConfigTemplateName]
	// parse filename template
	FilenameTemplate = template.Must(template.New("filenametemplate").Parse(FilenameTemplateString))

	filenames := flag.Args()
	if flag.NArg() < 1 {
		filenames = []string{"-"}
	}
	certs := ctb.ReadCertificates(filenames...)

	if len(certs) < 1 {
		log.Fatal("No certificates found")
	}
	log.Printf("Found and parsed %d certificates.\n", len(certs))

	var filenameBuf bytes.Buffer
	for _, cert := range certs {
		// generate filename
		filenameBuf.Reset()
		err = FilenameTemplate.Execute(&filenameBuf, cert)
		if err != nil {
			log.Fatal(err)
		}
		// re-encode to pem
		var pemdata bytes.Buffer
		err = pem.Encode(&pemdata, &pem.Block{
			Type:    "CERTIFICATE",
			Headers: nil,
			Bytes:   cert.Raw,
		})
		if err != nil {
			log.Fatal(err)
		}
		filename := strings.Join([]string{filenameBuf.String(), FilenameSuffix}, ".")
		if !configDryRun {
			err = os.WriteFile(filename, pemdata.Bytes(), 0644)
		}
		if err != nil {
			log.Fatal(err)
		} else {
			fmt.Fprintf(os.Stderr, "  Wrote »%s«\n", filename)
		}
	}
}
