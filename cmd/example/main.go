package main

import (
	"crypto/dsa" //nolint:all // we need to support all key types
	"crypto/ecdsa"
	"crypto/md5"
	"crypto/rsa"
	"crypto/sha1"
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"flag"
	"fmt"
	"log"

	ctb "gitlab.kit.edu/kit/kit-ca/CertificateToolbox"
)

func init() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
}

func SubjectSPKISHA256(cert *x509.Certificate) string {
	h := sha256.New()
	h.Write(cert.RawSubject)
	h.Write(cert.RawSubjectPublicKeyInfo)
	return fmt.Sprintf("%X", h.Sum(nil))
}

func SPKISHA256(cert *x509.Certificate) string {
	h := sha256.New()
	h.Write(cert.RawSubjectPublicKeyInfo)
	return fmt.Sprintf("%X", h.Sum(nil))
}

func PKPSHA256Hash(cert *x509.Certificate) string {
	h := sha256.New()
	switch pub := cert.PublicKey.(type) {
	case *rsa.PublicKey, *dsa.PublicKey, *ecdsa.PublicKey:
		der, _ := x509.MarshalPKIXPublicKey(pub)
		h.Write(der)
	default:
		return ""
	}
	return base64.StdEncoding.EncodeToString(h.Sum(nil))
}

func SHA256Hash(data []byte) string {
	h := sha256.Sum256(data)
	return fmt.Sprintf("%X", h[:])
}

func MD5Hash(data []byte) string {
	h := md5.Sum(data)
	return fmt.Sprintf("%X", h[:])
}

func SHA1Hash(data []byte) string {
	h := sha1.Sum(data)
	return fmt.Sprintf("%X", h[:])
}

func main() {
	flag.Parse()
	if flag.NArg() < 1 {
		log.Fatal("Please specify at least one certificate or directory")
	}
	certs := ctb.Readrecursive(flag.Args(), []string{".archive", ".git", ".gitignore"})
	if len(certs) < 1 {
		log.Fatal("No certificates found")
	}
	log.Printf("Found and parsed %d certificates.\n", len(certs))
	/*
		match, nomatch := ctb.Split(ctb.ValidAt(time.Now()), certs)
		log.Printf("%d are expired and %d are not.\n", len(nomatch), len(match))
		for _, cert := range match {
			fmt.Println(strings.Join(cert.EmailAddresses, "\n"))
		}
	*/
	for _, cert := range certs {
		h := sha256.New()
		h.Write(cert.RawSubject)
		h.Write(cert.RawSubjectPublicKeyInfo)
		fmt.Printf("%s\n", SubjectSPKISHA256(cert))
		fmt.Printf("%s\n", SPKISHA256(cert))
		fmt.Printf("%s\n", PKPSHA256Hash(cert))
		fmt.Printf("%s\n", SHA256Hash(cert.Raw))
		fmt.Printf("%s\n", MD5Hash(cert.Raw))
		fmt.Printf("%s\n", SHA1Hash(cert.Raw))
		h.Reset()
	}

}
