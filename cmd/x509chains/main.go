package main

import (
	"crypto/x509"
	"flag"
	"fmt"
	ctb "gitlab.kit.edu/kit/kit-ca/CertificateToolbox"
	"log"
	"strings"
	"time"
)

type FilenameArray []string

func (f *FilenameArray) String() string {
	return strings.Join(*f, ", ")
}

func (f *FilenameArray) Set(arg string) error {
	*f = append(*f, arg)
	return nil
}

func (f FilenameArray) IsBoolFlag() bool { return false }

var (
	Config struct {
		IntermediateCertFiles, IntermediateCertDirs FilenameArray
	}
)

func init() {
	flag.Var(&Config.IntermediateCertFiles, "i", "Filename(s) of intermediate certificates")
	flag.Var(&Config.IntermediateCertDirs, "d", "Directory name(s) containing intermediate certificates")
	flag.Parse()

	if flag.NArg() < 1 {
		log.Fatalln("no certificate filename(s) specified")
	}
}

func main() {
	// initialise trust pool with system trust pool
	trustedRoots, err := x509.SystemCertPool()
	if err != nil {
		log.Fatalln(err)
	}

	intermediatesCerts := ctb.ReadCertificates(Config.IntermediateCertFiles...)
	intermediatesPool := x509.NewCertPool()
	// load intermediates from files
	for _, cert := range intermediatesCerts {
		intermediatesPool.AddCert(cert)
	}
	// load intermediates from directories
	for _, cdir := range Config.IntermediateCertDirs {
		for _, cert := range ctb.ReadDirectory(cdir) {
			intermediatesPool.AddCert(cert)
		}
	}
	verifyOptions := x509.VerifyOptions{
		Intermediates: intermediatesPool,
		Roots:         trustedRoots,
		CurrentTime:   time.Time{},
	}

	// find all chains
	var (
		certFiles = flag.Args()
		certs     = ctb.Readrecursive(certFiles, nil)
		numErrors = 0
	)

	for numErrors < len(certs) {
		var (
			newRoots         = x509.NewCertPool()
			newIntermediates = x509.NewCertPool()
		)

		for _, c := range certs {
			chains, err := c.Verify(verifyOptions)
			if err != nil {
				log.Printf("%s: %s", c.Subject.String(), err)
				numErrors++
				break
			}
			fmt.Printf("===[ %s ]===\n", c.Subject.String())
			for _, chain := range chains {
				PrintChain(chain)
				//PrintRoots(chain)
				newRoots.AddCert(chain[len(chain)-1])
				fmt.Println()
			}
		}
		fmt.Println("============================================")
		verifyOptions.Intermediates = newIntermediates
		verifyOptions.Roots = newRoots
	}
}

func PrintChain(chain []*x509.Certificate) {
	for idx, cert := range chain {
		fmt.Printf("[%d] %s (%s)\n", idx, cert.Subject.String(), cert.NotAfter.String())
	}
}

func PrintRoots(chain []*x509.Certificate) {
	cert := chain[len(chain)-1]
	fmt.Printf("%s (%s)\n", cert.Subject.String(), cert.NotAfter.String())
}
