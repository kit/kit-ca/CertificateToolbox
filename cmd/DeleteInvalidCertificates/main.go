package main

import (
	"flag"
	"io"
	"log"

	"bytes"
	"crypto/x509"
	"fmt"
	"math/rand"
	"net/http"
	"os"
	"strings"
	"time"

	ctb "gitlab.kit.edu/kit/kit-ca/CertificateToolbox"
	"github.com/davecgh/go-spew/spew"
	"github.com/pkg/errors"
	"golang.org/x/crypto/ocsp"
)

var (
	removeFiles bool
)

func deleteOrPrint(filename string, reason string, remove bool) error {
	if remove {
		err := os.Remove(filename)
		if err == nil {
			fmt.Printf("Removed %s\n", filename)
		}
		return err
	}
	reason = strings.Replace(reason, "\n", "", -1)
	fmt.Printf("rm -f '%s' # %s\n", filename, reason)
	return nil
}

func CheckOCSP(cert *x509.Certificate) (int, error) {
	var (
		httpclient = &http.Client{
			Timeout: 10 * time.Second,
		}
		err    error
		issuer *x509.Certificate
	)
	// get signing certificate
	chains, err := cert.Verify(x509.VerifyOptions{
		KeyUsages:     []x509.ExtKeyUsage{x509.ExtKeyUsageAny},
		Intermediates: ctb.DFNIntermediates,
	})
	// TODO: if not found, try cert.IssuingCertificateURL
	if err != nil {
		return -1, err
	}
	if len(chains) > 0 && len(chains[0]) > 1 {
		issuer = chains[0][1]
	} else {
		return -1, errors.New("Unable to find signing certificate")
	}

	// generate OCSP request
	request, err := ocsp.CreateRequest(cert, issuer, nil)
	if err != nil {
		return -1, err
	}

	ocspURLs := cert.OCSPServer
	if len(ocspURLs) == 0 {
		return ocsp.Unknown, nil
	}
	// randomize OCSP server order
	rand.Shuffle(len(ocspURLs), func(i, j int) {
		ocspURLs[i], ocspURLs[j] = ocspURLs[j], ocspURLs[i]
	})

	// try all ocsp responders
	for _, url := range ocspURLs {
		// POST ocsp request
		resp, err := httpclient.Post(url, "application/ocsp-request", bytes.NewReader(request))
		if err != nil {
			continue
		}
		// check response on http layer
		if resp.StatusCode != 200 || resp.Header.Get("Content-Type") != "application/ocsp-response" {
			continue
		}
		responseBytes, err := io.ReadAll(resp.Body)
		if err != nil {
			continue
		}
		// read ocsp response
		response, err := ocsp.ParseResponse(responseBytes, issuer)
		if err != nil {
			continue
		}
		return response.Status, nil
	}
	// all requests failed, return Unknown
	return ocsp.Unknown, nil
}

func init() {
	flag.BoolVar(&removeFiles, "delete", false, "Delete invalid certificates from disk")
	flag.Parse()
	if flag.NArg() < 1 {
		log.Fatal("Please specify at least one certificate or directory")
	}

}

func main() {
	for _, filename := range flag.Args() {
		content, err := os.ReadFile(filename)
		if err != nil {
			log.Printf("Unable to read file »%s«, skipping: %s", filename, err)
			continue
		}
		certificates := ctb.ParseCertificate(content)
		if len(certificates) > 1 {
			log.Printf("Warning: »%s« contains more than one certificate, skipping", filename)
			continue
		}
		for _, cert := range certificates {
			// check expiration time
			if cert.NotAfter.Before(time.Now()) {
				err = deleteOrPrint(filename, "expired", removeFiles)
				if err != nil {
					log.Fatal(err)
				}
				continue
			}
			// check OCSP
			OCSPResult, err := CheckOCSP(cert)
			if err != nil {
				var certificateInvalidError x509.CertificateInvalidError
				switch {
				case errors.As(err, &certificateInvalidError):
					continue
				default:
					log.Printf("OCSP error, skipping »%s«: %s", filename, err)
					spew.Dump(err)
					continue
				}
			}
			if OCSPResult == ocsp.Revoked {
				err = deleteOrPrint(filename, "revoked", removeFiles)
				if err != nil {
					log.Fatal(err)
				}
			}
		}
	}
}
