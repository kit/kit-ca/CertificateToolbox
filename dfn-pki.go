package CertificateToolbox

import (
	"crypto/x509"
	"encoding/asn1"
	"regexp"
)

var (
	MatchSubjectPNEXT  = regexp.MustCompile(`^(?:PN|EXT)\s*(?:[:-])`)
	MatchSubjectNoMail = regexp.MustCompile(`(?i)(API|Teilnehmerservice|Login|Sign|Test|Demo|Apple)`)
	DFNIntermediates   = x509.NewCertPool()
)

func init() {
	DFNIntermediates.AppendCertsFromPEM([]byte(CertKITG1))
	DFNIntermediates.AppendCertsFromPEM([]byte(CertKITG2))
	DFNIntermediates.AppendCertsFromPEM([]byte(CertDFNG1))
	DFNIntermediates.AppendCertsFromPEM([]byte(CertDFNG2))
}

// Test for DFN profiles
var (
	// DFN: 802.1X User + User
	FilterIsProfileUser = And(
		KeyUsageFilter(x509.KeyUsageContentCommitment, x509.KeyUsageDigitalSignature, x509.KeyUsageKeyEncipherment),
		ExtKeyUsageFilter(x509.ExtKeyUsageClientAuth, x509.ExtKeyUsageEmailProtection),
		UnknownExtKeyUsageFilter())
	FilterIsProfileX8021XUser = FilterIsProfileUser
	// DNF: Code Signing
	FilterIsProfileCodeSigning = And(
		KeyUsageFilter(x509.KeyUsageDigitalSignature),
		ExtKeyUsageFilter(x509.ExtKeyUsageCodeSigning),
		UnknownExtKeyUsageFilter())
	// DFN: RA-Operator
	FilterIsProfileRAOperator = And(
		KeyUsageFilter(x509.KeyUsageContentCommitment, x509.KeyUsageDigitalSignature, x509.KeyUsageKeyEncipherment),
		ExtKeyUsageFilter(x509.ExtKeyUsageClientAuth, x509.ExtKeyUsageEmailProtection),
		UnknownExtKeyUsageFilter(ExtKeyUsageMicrosoftSmartcardLogon))
	// DNF: User Sign Only
	FilterIsProfileUserSign = And(
		KeyUsageFilter(x509.KeyUsageContentCommitment, x509.KeyUsageDigitalSignature),
		ExtKeyUsageFilter(x509.ExtKeyUsageEmailProtection),
		UnknownExtKeyUsageFilter())
	// DNF: User Sign and Logon
	FilterIsProfileUserSignAndLogon = And(
		KeyUsageFilter(x509.KeyUsageContentCommitment, x509.KeyUsageDigitalSignature),
		ExtKeyUsageFilter(x509.ExtKeyUsageClientAuth, x509.ExtKeyUsageEmailProtection),
		UnknownExtKeyUsageFilter())
	// DFN: User Encryption Only
	FilterIsProfileUserEncryption = And(
		KeyUsageFilter(x509.KeyUsageKeyEncipherment),
		ExtKeyUsageFilter(x509.ExtKeyUsageEmailProtection),
		UnknownExtKeyUsageFilter())
	// DFN: 802.1X Client
	FilterIsProfileX8021XClient = And(
		KeyUsageFilter(x509.KeyUsageDigitalSignature, x509.KeyUsageKeyEncipherment),
		ExtKeyUsageFilter(x509.ExtKeyUsageClientAuth),
		UnknownExtKeyUsageFilter())
	// DFN: LDAP Server, Mail Server, Radius Server, Shibboleth IdP SP, VoIP Server
	FilterIsProfileLDAPServer = And(
		KeyUsageFilter(x509.KeyUsageDigitalSignature, x509.KeyUsageKeyEncipherment),
		ExtKeyUsageFilter(x509.ExtKeyUsageClientAuth, x509.ExtKeyUsageServerAuth),
		UnknownExtKeyUsageFilter())
	FilterIsProfileMailServer            = FilterIsProfileLDAPServer
	FilterIsProfileRadiusServer          = FilterIsProfileLDAPServer
	FilterIsProfileShibbolethIdPSPServer = FilterIsProfileLDAPServer
	FilterIsProfileVoIPServer            = FilterIsProfileLDAPServer
	// DFN: VPN Server, Web Server
	FilterIsProfileVPNServer = And(
		KeyUsageFilter(x509.KeyUsageDigitalSignature, x509.KeyUsageKeyEncipherment),
		ExtKeyUsageFilter(x509.ExtKeyUsageServerAuth),
		UnknownExtKeyUsageFilter(),
		Not(ExtensionPresentFilter(asn1.ObjectIdentifier{1, 3, 6, 1, 5, 5, 7, 1, 24})))
	FilterIsProfileWebServer = FilterIsProfileVPNServer
	// DFN: Web Server Must Staple
	FilterIsProfileWebServerMustStaple = And(
		KeyUsageFilter(x509.KeyUsageDigitalSignature, x509.KeyUsageKeyEncipherment),
		ExtKeyUsageFilter(x509.ExtKeyUsageServerAuth),
		UnknownExtKeyUsageFilter(),
		ExtensionPresentFilter(asn1.ObjectIdentifier{1, 3, 6, 1, 5, 5, 7, 1, 24}))
	FilterIsProfileDomainController = And(
		KeyUsageFilter(x509.KeyUsageDigitalSignature, x509.KeyUsageKeyEncipherment),
		ExtKeyUsageFilter(x509.ExtKeyUsageClientAuth, x509.ExtKeyUsageServerAuth),
		UnknownExtKeyUsageFilter(ExtKeyUsageMicrosoftSmartcardLogon, ExtKeyUsageKDCAuth),
		ExtensionPresentFilter(asn1.ObjectIdentifier{1, 3, 6, 1, 4, 1, 311, 20, 2}))
	FilterIsProfileExchangeServer = And(
		KeyUsageFilter(x509.KeyUsageDigitalSignature, x509.KeyUsageKeyEncipherment),
		ExtKeyUsageFilter(x509.ExtKeyUsageClientAuth, x509.ExtKeyUsageServerAuth, x509.ExtKeyUsageEmailProtection),
		UnknownExtKeyUsageFilter())
)

// Test for DFN classes
var (
	FilterIsServer = Or(
		FilterIsProfileX8021XClient,
		FilterIsProfileLDAPServer,
		FilterIsProfileVPNServer,
		FilterIsProfileWebServerMustStaple,
		FilterIsProfileDomainController,
		FilterIsProfileExchangeServer)
	FilterIsNutzer = Or(
		FilterIsProfileUser,
		FilterIsProfileCodeSigning,
		FilterIsProfileRAOperator,
		FilterIsProfileUserSign,
		FilterIsProfileUserSignAndLogon,
		FilterIsProfileUserEncryption)
	FilterIsPseudonym = CommonNameRegexpMatch("^PN {0,1}(:|-)")
	FilterIsGroup     = CommonNameRegexpMatch("^GRP {0,1}(:|-)")
	FilterIsExternal  = CommonNameRegexpMatch("^EXT {0,1}(:|-)")
)

const CertKITG1 = `-----BEGIN CERTIFICATE-----
MIIFlDCCBHygAwIBAgIHF6/3byMi6TANBgkqhkiG9w0BAQsFADBaMQswCQYDVQQG
EwJERTETMBEGA1UEChMKREZOLVZlcmVpbjEQMA4GA1UECxMHREZOLVBLSTEkMCIG
A1UEAxMbREZOLVZlcmVpbiBQQ0EgR2xvYmFsIC0gRzAxMB4XDTE0MDYwNTE0MDgz
MVoXDTE5MDcwOTIzNTkwMFowgb8xCzAJBgNVBAYTAkRFMRswGQYDVQQIExJCYWRl
bi1XdWVydHRlbWJlcmcxEjAQBgNVBAcTCUthcmxzcnVoZTEqMCgGA1UEChMhS2Fy
bHNydWhlIEluc3RpdHV0ZSBvZiBUZWNobm9sb2d5MScwJQYDVQQLEx5TdGVpbmJ1
Y2ggQ2VudHJlIGZvciBDb21wdXRpbmcxDzANBgNVBAMTBktJVC1DQTEZMBcGCSqG
SIb3DQEJARYKY2FAa2l0LmVkdTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoC
ggEBAMyyqIqJooBBUSkvbR08HQh6NNE+tqNErubDBemoGj8A803wg+1xvXqkhO1v
nBABp5U++7xgD5uLlTcOb/F4UFrJBBy9QCnThzCb+eqG+94Iw6m8LjWqCKor9lKw
NCUit7vzNAOCd0r0iNCYV2/xcK8tHSHw3mGXgvwfY0F9j14nUEg9nd5kPq0Aw9iQ
HDuYnOz3A8NVTpXpTCziQTCZJQYETDsC+nynQN2svAmAV3V1HJRe/AD6idy/6HN0
EtAIOPlE1px14NFS4SZqUk/F4vD8pxSz/YUwbCGK4IH37V9bcbQmA8NNNKBtFLqX
19ovuQn32lRwSW/Cz3Hj+zSRP3MCAwEAAaOCAfcwggHzMBIGA1UdEwEB/wQIMAYB
Af8CAQEwDgYDVR0PAQH/BAQDAgEGMBEGA1UdIAQKMAgwBgYEVR0gADAdBgNVHQ4E
FgQUH3Rl9JodevYx6d9hG3MrDW3QM0kwHwYDVR0jBBgwFoAUSbfGz+g9H3/qRHsT
KffxCnA+3mQwFQYDVR0RBA4wDIEKY2FAa2l0LmVkdTCBiAYDVR0fBIGAMH4wPaA7
oDmGN2h0dHA6Ly9jZHAxLnBjYS5kZm4uZGUvZ2xvYmFsLXJvb3QtY2EvcHViL2Ny
bC9jYWNybC5jcmwwPaA7oDmGN2h0dHA6Ly9jZHAyLnBjYS5kZm4uZGUvZ2xvYmFs
LXJvb3QtY2EvcHViL2NybC9jYWNybC5jcmwwgdcGCCsGAQUFBwEBBIHKMIHHMDMG
CCsGAQUFBzABhidodHRwOi8vb2NzcC5wY2EuZGZuLmRlL09DU1AtU2VydmVyL09D
U1AwRwYIKwYBBQUHMAKGO2h0dHA6Ly9jZHAxLnBjYS5kZm4uZGUvZ2xvYmFsLXJv
b3QtY2EvcHViL2NhY2VydC9jYWNlcnQuY3J0MEcGCCsGAQUFBzAChjtodHRwOi8v
Y2RwMi5wY2EuZGZuLmRlL2dsb2JhbC1yb290LWNhL3B1Yi9jYWNlcnQvY2FjZXJ0
LmNydDANBgkqhkiG9w0BAQsFAAOCAQEAOhYm/9RXIH2d+yjKb3Eqnxruw4trhdI9
T/GsoXgOndR2Up3MMrYm/MLOIXnRKIfnhm4puwpRrphfAeJYc7u2xRbb6vpydt/o
UZy/KXJ46161Cygq8HZsyEQbR+fUHj72Tki5cmlxBIeIpmFFr+Wo1Y73mEoe+G1Q
7x2VNi2VNpISa/pEBx/DYlxgqIHYYL3uHdinCKYMLXmx/PBh2eFN1JcXngJU+DAc
8vzlxH9gjsqVC2O8jrLPXiaZrt99g3pbtQEM89z/l6aJgiDFvAQEVnmit+S8+194
lS5PcUjtEGYTyY+X9t61591nOoXUXfnt6aum4Yf2Blsmn3xKCu5CqA==
-----END CERTIFICATE-----`

const CertDFNG1 = `-----BEGIN CERTIFICATE-----
MIIE1TCCA72gAwIBAgIIUE7G9T0RtGQwDQYJKoZIhvcNAQELBQAwcTELMAkGA1UE
BhMCREUxHDAaBgNVBAoTE0RldXRzY2hlIFRlbGVrb20gQUcxHzAdBgNVBAsTFlQt
VGVsZVNlYyBUcnVzdCBDZW50ZXIxIzAhBgNVBAMTGkRldXRzY2hlIFRlbGVrb20g
Um9vdCBDQSAyMB4XDTE0MDcyMjEyMDgyNloXDTE5MDcwOTIzNTkwMFowWjELMAkG
A1UEBhMCREUxEzARBgNVBAoTCkRGTi1WZXJlaW4xEDAOBgNVBAsTB0RGTi1QS0kx
JDAiBgNVBAMTG0RGTi1WZXJlaW4gUENBIEdsb2JhbCAtIEcwMTCCASIwDQYJKoZI
hvcNAQEBBQADggEPADCCAQoCggEBAOmbw2eF+Q2u9Y1Uw5ZQNT1i6W5M7ZTXAFuV
InTUIOs0j9bswDEEC5mB4qYU0lKgKCOEi3SJBF5b4OJ4wXjLFssoNTl7LZBF0O2g
AHp8v0oOGwDDhulcKzERewzzgiRDjBw4i2poAJru3E94q9LGE5t2re7eJujvAa90
D8EJovZrzr3TzRQwT/Xl46TIYpuCGgMnMA0CZWBN7dEJIyqWNVgn03bGcbaQHcTt
/zWGfW8zs9sPxRHCioOhlF1Ba9jSEPVM/cpRrNm975KDu9rrixZWVkPP4dUTPaYf
JzDNSVTbyRM0mnF1xWzqpwuY+SGdJ68+ozk5SGqMrcmZ+8MS8r0CAwEAAaOCAYYw
ggGCMA4GA1UdDwEB/wQEAwIBBjAdBgNVHQ4EFgQUSbfGz+g9H3/qRHsTKffxCnA+
3mQwHwYDVR0jBBgwFoAUMcN5G7r1U9cX4Il6LRdsCrMrnTMwEgYDVR0TAQH/BAgw
BgEB/wIBAjBiBgNVHSAEWzBZMBEGDysGAQQBga0hgiwBAQQCAjARBg8rBgEEAYGt
IYIsAQEEAwAwEQYPKwYBBAGBrSGCLAEBBAMBMA8GDSsGAQQBga0hgiwBAQQwDQYL
KwYBBAGBrSGCLB4wPgYDVR0fBDcwNTAzoDGgL4YtaHR0cDovL3BraTAzMzYudGVs
ZXNlYy5kZS9ybC9EVF9ST09UX0NBXzIuY3JsMHgGCCsGAQUFBwEBBGwwajAsBggr
BgEFBQcwAYYgaHR0cDovL29jc3AwMzM2LnRlbGVzZWMuZGUvb2NzcHIwOgYIKwYB
BQUHMAKGLmh0dHA6Ly9wa2kwMzM2LnRlbGVzZWMuZGUvY3J0L0RUX1JPT1RfQ0Ff
Mi5jZXIwDQYJKoZIhvcNAQELBQADggEBAGMgKP2cIYZyvjlGWTkyJbypAZsNzMp9
QZyGbQpuLLMTWXWxM5IbYScW/8Oy1TWC+4QqAUm9ZrtmL7LCBl1uP27jAVpbykNj
XJW24TGnH9UHX03mZYJOMvnDfHpLzU1cdO4h8nUC7FI+0slq05AjbklnNb5/TVak
7Mwvz7ehl6hyPsm8QNZapAg91ryCw7e3Mo6xLI5qbbc1AhnP9TlEWGOnJAAQsLv8
Tq9uLzi7pVdJP9huUG8sl5bcHUaaZYnPrszy5dmfU7M+oS+SqdgLxoQfBMbrHuif
fbV7pQLxJMUkYxE0zFqTICp5iDolQpCpZTt8htMSFSMp/CzazDlbVBc=
-----END CERTIFICATE-----`

const CertKITG2 = `-----BEGIN CERTIFICATE-----
MIIFnjCCBIagAwIBAgIMHDrUjCTtki6w9JCuMA0GCSqGSIb3DQEBCwUAMIGVMQsw
CQYDVQQGEwJERTFFMEMGA1UEChM8VmVyZWluIHp1ciBGb2VyZGVydW5nIGVpbmVz
IERldXRzY2hlbiBGb3JzY2h1bmdzbmV0emVzIGUuIFYuMRAwDgYDVQQLEwdERk4t
UEtJMS0wKwYDVQQDEyRERk4tVmVyZWluIENlcnRpZmljYXRpb24gQXV0aG9yaXR5
IDIwHhcNMTYxMTAzMTUyNTQ4WhcNMzEwMjIyMjM1OTU5WjB7MQswCQYDVQQGEwJE
RTEbMBkGA1UECAwSQmFkZW4tV3VlcnR0ZW1iZXJnMRIwEAYDVQQHDAlLYXJsc3J1
aGUxKjAoBgNVBAoMIUthcmxzcnVoZSBJbnN0aXR1dGUgb2YgVGVjaG5vbG9neTEP
MA0GA1UEAwwGS0lULUNBMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA
1cwA90GCGWXXCQ+V4MkCc9CaPcYU0hrUc0mTyvbQ3DuOMey+w5aFI8JkmAD5CHsj
jDWe/uuANc6OR828xLoN6EV4AsNMhD9HMelYZrGuBS2nsOVlGzIIf8t+RX7pDVQg
Dc/UvczziO45HmLCoYeUDcqn9Qho8Jnuh4nXJdPCRB4UvcwtRCpYkgCeC+Llxfe4
IpdnvABTX1kwYR4TVmnAg3Mel5tLoV+WcxTdwJMpsbjFz8P11qMta53Nactef4gx
BWMSEpDlnGL+swzkUfd1s+3NQkFKBbAZ6ehZivOZc4ah4iRxB1T/5v2Tza+Hyao0
4f5SbkkD2Y1p0Z5vdMVEPwIDAQABo4ICBTCCAgEwEgYDVR0TAQH/BAgwBgEB/wIB
ATAOBgNVHQ8BAf8EBAMCAQYwKQYDVR0gBCIwIDANBgsrBgEEAYGtIYIsHjAPBg0r
BgEEAYGtIYIsAQEEMB0GA1UdDgQWBBQEGr8ck5E909k9sN4TI+WacPQuCDAfBgNV
HSMEGDAWgBST49gyJtrV8UqlkUrg6kviogzP4TCBjwYDVR0fBIGHMIGEMECgPqA8
hjpodHRwOi8vY2RwMS5wY2EuZGZuLmRlL2dsb2JhbC1yb290LWcyLWNhL3B1Yi9j
cmwvY2FjcmwuY3JsMECgPqA8hjpodHRwOi8vY2RwMi5wY2EuZGZuLmRlL2dsb2Jh
bC1yb290LWcyLWNhL3B1Yi9jcmwvY2FjcmwuY3JsMIHdBggrBgEFBQcBAQSB0DCB
zTAzBggrBgEFBQcwAYYnaHR0cDovL29jc3AucGNhLmRmbi5kZS9PQ1NQLVNlcnZl
ci9PQ1NQMEoGCCsGAQUFBzAChj5odHRwOi8vY2RwMS5wY2EuZGZuLmRlL2dsb2Jh
bC1yb290LWcyLWNhL3B1Yi9jYWNlcnQvY2FjZXJ0LmNydDBKBggrBgEFBQcwAoY+
aHR0cDovL2NkcDIucGNhLmRmbi5kZS9nbG9iYWwtcm9vdC1nMi1jYS9wdWIvY2Fj
ZXJ0L2NhY2VydC5jcnQwDQYJKoZIhvcNAQELBQADggEBAH+/QKluUYGThHAZeh5I
erhZdIN9B488jjjexsfs1p0IHjKAElUaxHdjTfHc2zENqfPtc+EKRJL04MOUWJj7
8BSR2MOWu8WrOVXYmIZAJAzH1L1eFNR6cem3d1nsnjqtqw21EQ+/O/kXb9lIK/sv
YDwudINFcdWE5UmOic9ZHSwFPb4oEeM97tG18WQkiVzKF1cM6nzLHWp78uRLwD1Q
ek1y7eX5gc8iQSvv33wTlh0ppmbDqfaMTR/X/ED7SFfd5S9CF07EXj/XWlv7ps2g
afR793FaPrPloGvghtC7pKT8uOMNBXC02Xq6drN6JlU+5k+bbSRcAd40Bqn6vyYA
RKQ=
-----END CERTIFICATE-----`

const CertDFNG2 = `-----BEGIN CERTIFICATE-----
MIIFEjCCA/qgAwIBAgIJAOML1fivJdmBMA0GCSqGSIb3DQEBCwUAMIGCMQswCQYD
VQQGEwJERTErMCkGA1UECgwiVC1TeXN0ZW1zIEVudGVycHJpc2UgU2VydmljZXMg
R21iSDEfMB0GA1UECwwWVC1TeXN0ZW1zIFRydXN0IENlbnRlcjElMCMGA1UEAwwc
VC1UZWxlU2VjIEdsb2JhbFJvb3QgQ2xhc3MgMjAeFw0xNjAyMjIxMzM4MjJaFw0z
MTAyMjIyMzU5NTlaMIGVMQswCQYDVQQGEwJERTFFMEMGA1UEChM8VmVyZWluIHp1
ciBGb2VyZGVydW5nIGVpbmVzIERldXRzY2hlbiBGb3JzY2h1bmdzbmV0emVzIGUu
IFYuMRAwDgYDVQQLEwdERk4tUEtJMS0wKwYDVQQDEyRERk4tVmVyZWluIENlcnRp
ZmljYXRpb24gQXV0aG9yaXR5IDIwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEK
AoIBAQDLYNf/ZqFBzdL6h5eKc6uZTepnOVqhYIBHFU6MlbLlz87TV0uNzvhWbBVV
dgfqRv3IA0VjPnDUq1SAsSOcvjcoqQn/BV0YD8SYmTezIPZmeBeHwp0OzEoy5xad
rg6NKXkHACBU3BVfSpbXeLY008F0tZ3pv8B3Teq9WQfgWi9sPKUA3DW9ZQ2PfzJt
8lpqS2IB7qw4NFlFNkkF2njKam1bwIFrEczSPKiL+HEayjvigN0WtGd6izbqTpEp
PbNRXK2oDL6dNOPRDReDdcQ5HrCUCxLx1WmOJfS4PSu/wI7DHjulv1UQqyquF5de
M87I8/QJB+MChjFGawHFEAwRx1npAgMBAAGjggF0MIIBcDAOBgNVHQ8BAf8EBAMC
AQYwHQYDVR0OBBYEFJPj2DIm2tXxSqWRSuDqS+KiDM/hMB8GA1UdIwQYMBaAFL9Z
IDYAeaCgImuM1fJh0rgsy4JKMBIGA1UdEwEB/wQIMAYBAf8CAQIwMwYDVR0gBCww
KjAPBg0rBgEEAYGtIYIsAQEEMA0GCysGAQQBga0hgiweMAgGBmeBDAECAjBMBgNV
HR8ERTBDMEGgP6A9hjtodHRwOi8vcGtpMDMzNi50ZWxlc2VjLmRlL3JsL1RlbGVT
ZWNfR2xvYmFsUm9vdF9DbGFzc18yLmNybDCBhgYIKwYBBQUHAQEEejB4MCwGCCsG
AQUFBzABhiBodHRwOi8vb2NzcDAzMzYudGVsZXNlYy5kZS9vY3NwcjBIBggrBgEF
BQcwAoY8aHR0cDovL3BraTAzMzYudGVsZXNlYy5kZS9jcnQvVGVsZVNlY19HbG9i
YWxSb290X0NsYXNzXzIuY2VyMA0GCSqGSIb3DQEBCwUAA4IBAQCHC/8+AptlyFYt
1juamItxT9q6Kaoh+UYu9bKkD64ROHk4sw50unZdnugYgpZi20wz6N35at8yvSxM
R2BVf+d0a7Qsg9h5a7a3TVALZge17bOXrerufzDmmf0i4nJNPoRb7vnPmep/11I5
LqyYAER+aTu/de7QCzsazeX3DyJsR4T2pUeg/dAaNH2t0j13s+70103/w+jlkk9Z
PpBHEEqwhVjAb3/4ru0IQp4e1N8ULk2PvJ6Uw+ft9hj4PEnnJqinNtgs3iLNi4LY
2XjiVRKjO4dEthEL1QxSr2mMDwbf0KJTi1eYe8/9ByT0/L3D/UqSApcb8re2z2WK
GqK1chk5
-----END CERTIFICATE-----`
