package CertificateToolbox

import (
	"crypto/x509"
	"encoding/asn1"
	"sort"
	"strings"
)

func ExtKeyUsageStringer(ExtKeyUsage []x509.ExtKeyUsage) string {
	var extkeyusage []string

	for _, eku := range ExtKeyUsage {
		switch eku {
		case x509.ExtKeyUsageAny:
			extkeyusage = append(extkeyusage, "ExtKeyUsageAny")
		case x509.ExtKeyUsageServerAuth:
			extkeyusage = append(extkeyusage, "ExtKeyUsageServerAuth")
		case x509.ExtKeyUsageClientAuth:
			extkeyusage = append(extkeyusage, "ExtKeyUsageClientAuth")
		case x509.ExtKeyUsageCodeSigning:
			extkeyusage = append(extkeyusage, "ExtKeyUsageCodeSigning")
		case x509.ExtKeyUsageEmailProtection:
			extkeyusage = append(extkeyusage, "ExtKeyUsageEmailProtection")
		case x509.ExtKeyUsageIPSECEndSystem:
			extkeyusage = append(extkeyusage, "ExtKeyUsageIPSECEndSystem")
		case x509.ExtKeyUsageIPSECTunnel:
			extkeyusage = append(extkeyusage, "ExtKeyUsageIPSECTunnel")
		case x509.ExtKeyUsageIPSECUser:
			extkeyusage = append(extkeyusage, "ExtKeyUsageIPSECUser")
		case x509.ExtKeyUsageTimeStamping:
			extkeyusage = append(extkeyusage, "ExtKeyUsageTimeStamping")
		case x509.ExtKeyUsageOCSPSigning:
			extkeyusage = append(extkeyusage, "ExtKeyUsageOCSPSigning")
		case x509.ExtKeyUsageMicrosoftServerGatedCrypto:
			extkeyusage = append(extkeyusage, "ExtKeyUsageMicrosoftServerGatedCrypto")
		case x509.ExtKeyUsageNetscapeServerGatedCrypto:
			extkeyusage = append(extkeyusage, "ExtKeyUsageNetscapeServerGatedCrypto")
		}
	}
	sort.Strings(extkeyusage)
	return strings.Join(extkeyusage, ",")
}

func KeyUsageStringer(KeyUsage x509.KeyUsage) string {
	var ku []string
	KeyUsageNames := map[x509.KeyUsage]string{
		x509.KeyUsageDigitalSignature:  "KeyUsageDigitalSignature",
		x509.KeyUsageContentCommitment: "KeyUsageContentCommitment",
		x509.KeyUsageKeyEncipherment:   "KeyUsageKeyEncipherment",
		x509.KeyUsageDataEncipherment:  "KeyUsageDataEncipherment",
		x509.KeyUsageKeyAgreement:      "KeyUsageKeyAgreement",
		x509.KeyUsageCertSign:          "KeyUsageCertSign",
		x509.KeyUsageCRLSign:           "KeyUsageCRLSign",
		x509.KeyUsageEncipherOnly:      "KeyUsageEncipherOnly",
		x509.KeyUsageDecipherOnly:      "KeyUsageDecipherOnly",
	}

	for keyusage, name := range KeyUsageNames {
		if KeyUsage&keyusage != 0 {
			ku = append(ku, name)
		}
	}

	sort.Strings(ku)
	return strings.Join(ku, ",")
}

func UnknownExtKeyUsageStringer(UnknownExtKeyUsage []asn1.ObjectIdentifier) string {
	var ueku []string

	for _, unknownextkeyusage := range UnknownExtKeyUsage {
		switch {
		case unknownextkeyusage.Equal(asn1.ObjectIdentifier{1, 3, 6, 1, 4, 1, 311, 20, 2, 2}):
			ueku = append(ueku, "ExtKeyUsageMicrosofSmartcardLogon")
		case unknownextkeyusage.Equal(asn1.ObjectIdentifier{1, 3, 6, 1, 5, 2, 3, 5}):
			ueku = append(ueku, "ExtKeyUsageKDCAuth")
		default:
			ueku = append(ueku, unknownextkeyusage.String())
		}
	}

	sort.Strings(ueku)
	return strings.Join(ueku, ",")
}

func ProfileStringer(c x509.Certificate) string {
	return strings.Join([]string{
		KeyUsageStringer(c.KeyUsage),
		ExtKeyUsageStringer(c.ExtKeyUsage),
		UnknownExtKeyUsageStringer(c.UnknownExtKeyUsage),
	}, ",")
}
