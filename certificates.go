package CertificateToolbox

import (
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
)

// Parse certificate blob as PEM or DER, return all certificates.
// Non-certificates are silently ignored.
func ParseCertificate(input []byte) []*x509.Certificate {
	var (
		allcerts = make([]*x509.Certificate, 0)
		block    *pem.Block
	)
	// loop over all PEM sections
	// try decoding as DER
	certs, err := x509.ParseCertificates(input)
	if err == nil {
		// found cert: append and return
		allcerts = append(allcerts, certs...)
		return allcerts
	}
	// try decoding as PEM
	for block, input = pem.Decode(input); block != nil; block, input = pem.Decode(input) {
		// process only certificates
		if block.Type == "CERTIFICATE" {
			certs, err := x509.ParseCertificates(block.Bytes)
			if err == nil {
				allcerts = append(allcerts, certs...)
			}
		}
	}
	return allcerts
}

// Parse certificates from filenames
func ReadCertificates(filenames ...string) []*x509.Certificate {
	var (
		allcerts = make([]*x509.Certificate, 0)
		content  []byte
		err      error
	)

	for _, filename := range filenames {
		// read certificate (treat "-" as stdin)
		if filename == "-" {
			content, err = ioutil.ReadAll(os.Stdin)
		} else {
			content, err = ioutil.ReadFile(filename)
		}
		if err == nil {
			allcerts = append(allcerts, ParseCertificate(content)...)
		}
	}
	return allcerts
}

// Parse certificates from all files in path
func ReadDirectory(path string) []*x509.Certificate {
	var allcerts = make([]*x509.Certificate, 0)

	files, err := ioutil.ReadDir(path)
	if err != nil {
		log.Print(err)
	}
	for _, certfile := range files {
		if certfile.IsDir() {
			continue
		}
		content, err := ioutil.ReadFile(filepath.Join(path, certfile.Name()))
		if err != nil {
			continue
		}
		allcerts = append(allcerts, ParseCertificate(content)...)
	}
	return allcerts
}

// Parse certificates for files or paths, recursively descending into directories, ignoring most errors
func Readrecursive(fileorpath, ignorepath []string) []*x509.Certificate {
	var (
		allcerts  = make([]*x509.Certificate, 0)
		ignoremap = make(map[string]bool)
	)

	for _, path := range ignorepath {
		ignoremap[path] = true
	}

	for _, fsobject := range fileorpath {
		fostat, err := os.Lstat(fsobject)
		if err != nil {
			continue
		}
		if fostat.IsDir() {
			err = filepath.Walk(fsobject, func(path string, info os.FileInfo, err error) error {
				fmt.Println(path)
				if ignoremap[info.Name()] {
					return filepath.SkipDir
				}
				if info.IsDir() {
					return nil
				}
				newcerts := ReadCertificates(path)
				allcerts = append(allcerts, newcerts...)
				return nil
			})
			if err != nil {
				return nil
			}
		} else {
			allcerts = append(allcerts, ReadCertificates(fsobject)...)
		}
	}

	return allcerts
}
