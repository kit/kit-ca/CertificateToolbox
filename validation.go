package CertificateToolbox

import (
	"bytes"
	"crypto/x509"
	"errors"
	"io/ioutil"
	"math/rand"
	"net/http"
	"time"

	"golang.org/x/crypto/blake2b"
	"golang.org/x/crypto/ocsp"
)

const (
	HttpClientTimeout = time.Second * 10
)

// Check a certificate's validity against its CRL
func CheckCRL(cert *x509.Certificate) (int, error) {
	var (
		httpclient = &http.Client{
			Timeout: HttpClientTimeout,
		}
		err   error
		resp  *http.Response
		body  []byte
		cache = make(map[string]time.Time)
	)
	// randomize crl urls
	crlurls := cert.CRLDistributionPoints
	rand.Shuffle(len(crlurls), func(i, j int) {
		crlurls[i], crlurls[j] = crlurls[j], crlurls[i]
	})
	for _, url := range crlurls {
		// get crl
		resp, err = httpclient.Get(url)
		if err != nil {
			continue
		}
		body, err = ioutil.ReadAll(resp.Body)
		_ = resp.Body.Close()
		if err != nil {
			continue
		}
		// parse crl
		crlcerts, err := x509.ParseCRL(body)
		if err != nil {
			continue
		}
		//spew.Dump(crlcerts.TBSCertList.ThisUpdate)
		for _, rejected := range crlcerts.TBSCertList.RevokedCertificates {
			cache[rejected.SerialNumber.String()] = rejected.RevocationTime
		}
		if _, ok := cache[cert.SerialNumber.String()]; ok {
			return ocsp.Revoked, nil
		} else {
			return ocsp.Unknown, nil
		}
	}
	return ocsp.Unknown, nil
}

func hashCert(cert *x509.Certificate) string {
	rawhash := blake2b.Sum256(cert.Raw)
	return string(rawhash[:])
}

// untested…
func RetrieveChain(cert *x509.Certificate) (chains []*x509.Certificate, err error) {
	var (
		httpclient = &http.Client{
			Timeout: HttpClientTimeout,
		}
		seen        = make(map[string]bool)
		body        []byte
		certqueue   []*x509.Certificate
		certificate *x509.Certificate
	)

	certqueue = append(certqueue, cert)

	for {
		if len(certqueue) == 0 {
			break
		} else {
			// pop certificate from end of queue
			certificate, certqueue = certqueue[len(certqueue)-1], certqueue[:len(certqueue)-1]
		}
		// no issuer, must (hopefully) be root
		if len(certificate.IssuingCertificateURL) == 0 {
			return chains, nil
		}
		// get parent cert
		issuerURLs := certificate.IssuingCertificateURL

		// randomize URLs
		rand.Shuffle(len(issuerURLs), func(i, j int) {
			issuerURLs[i], issuerURLs[j] = issuerURLs[j], issuerURLs[i]
		})
		// try all URLs
		for _, url := range issuerURLs {
			resp, err := httpclient.Get(url)
			if err != nil {
				continue
			}
			body, err = ioutil.ReadAll(resp.Body)
			_ = resp.Body.Close()
			if err != nil {
				continue
			} else {
				// certificate retrieved
				break
			}
		}

		issuerCerts := ParseCertificate(body)
		for _, ic := range issuerCerts {
			certhash := hashCert(ic)
			_, known := seen[certhash]
			// only add unknown certificates (prevent infinite loops)
			if !known {
				// mark certificate as seen
				seen[certhash] = true
				// add certificates to chain
				chains = append(chains, ic)
				// add certificate to queue
				certqueue = append([]*x509.Certificate{ic}, certqueue...)
			}
		}
	}

	return certqueue, nil
}

func CheckOCSP(cert *x509.Certificate) (int, error) {
	var (
		httpclient = &http.Client{
			Timeout: HttpClientTimeout,
		}
		err    error
		issuer *x509.Certificate
	)
	// get signing certificate
	chains, err := cert.Verify(x509.VerifyOptions{
		KeyUsages:     []x509.ExtKeyUsage{x509.ExtKeyUsageAny},
		Intermediates: DFNIntermediates,
	})
	// TODO: if not found, try cert.IssuingCertificateURL
	if err != nil {
		return -1, err
	}
	if len(chains) > 0 && len(chains[0]) > 1 {
		issuer = chains[0][1]
	} else {
		return -1, errors.New("Unable to find signing certificate")
	}

	// generate OCSP request
	request, err := ocsp.CreateRequest(cert, issuer, nil)
	if err != nil {
		return -1, err
	}

	ocspurls := cert.OCSPServer
	if len(ocspurls) == 0 {
		return ocsp.Unknown, nil
	}
	// randomize OCSP server order
	rand.Shuffle(len(ocspurls), func(i, j int) {
		ocspurls[i], ocspurls[j] = ocspurls[j], ocspurls[i]
	})

	// try all ocsp responders
	for _, url := range ocspurls {
		// POST ocsp request
		resp, err := httpclient.Post(url, "application/ocsp-request", bytes.NewReader(request))
		if err != nil {
			continue
		}
		// check response on http layer
		if resp.StatusCode != 200 || resp.Header.Get("Content-Type") != "application/ocsp-response" {
			continue
		}
		responsebytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			continue
		}
		// read ocsp response
		response, err := ocsp.ParseResponse(responsebytes, issuer)
		if err != nil {
			continue
		}
		return response.Status, nil
	}
	// all requests failed, return Unknown
	return ocsp.Unknown, nil
}

// good until proven otherwise
func FilterOCSPLax(cert *x509.Certificate) bool {
	result, err := CheckOCSP(cert)
	if err != nil {
		// possible server error, assume certificate is good
		return true
	}
	switch result {
	case ocsp.Good:
		return true
	case ocsp.Revoked:
		return false
	case ocsp.Unknown:
		return true // we don't know, assume good
	case ocsp.ServerFailed:
		return true // we don't know, assume good
	default:
		return true // unknown error, assume good
	}
}

// bad until proven otherwise
func FilterOCSPStrict(cert *x509.Certificate) bool {
	result, err := CheckOCSP(cert)
	if err != nil {
		return false
	}
	switch result {
	case ocsp.Good:
		return true
	case ocsp.Revoked:
		return false
	case ocsp.Unknown:
		return false
	case ocsp.ServerFailed:
		return false
	default:
		return false
	}
}
