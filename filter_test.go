package CertificateToolbox

import (
	"crypto/x509"
	"encoding/pem"
	"io/ioutil"
	"testing"
)

// load certificate from PEM file. Panic on error.
func loadCertfile(filename string) *x509.Certificate {
	content, err := ioutil.ReadFile(filename)
	if err != nil {
		panic(err)
	}

	// read first pem block
	block, _ := pem.Decode(content)
	if block == nil {
		panic("%s does not contain a PEM block" + filename)
	}

	if block.Type == "CERTIFICATE" {
		cert, err := x509.ParseCertificates(block.Bytes)
		if err != nil {
			panic(err)
		}
		// return first certificate
		return cert[0]
	} else {
		panic(filename + " does not contain a certificate, but a " + block.Type)
	}
}

type filterTest struct {
	filter   CertSelector
	input    *x509.Certificate
	expected bool
}

var (
	emptyCert              = &x509.Certificate{}
	simpleCombinationTests = [...]filterTest{
		// definition
		filterTest{AlwaysMatch, emptyCert, true},
		filterTest{NeverMatch, emptyCert, false},
		// NOT
		filterTest{Not(AlwaysMatch), emptyCert, false},
		filterTest{Not(NeverMatch), emptyCert, true},
		// AND
		filterTest{And(AlwaysMatch), emptyCert, true},
		filterTest{And(AlwaysMatch, AlwaysMatch), emptyCert, true},
		filterTest{And(AlwaysMatch, AlwaysMatch, AlwaysMatch), emptyCert, true},
		filterTest{And(AlwaysMatch, NeverMatch), emptyCert, false},
		filterTest{And(NeverMatch, AlwaysMatch), emptyCert, false},
		filterTest{And(NeverMatch), emptyCert, false},
		filterTest{And(NeverMatch, NeverMatch), emptyCert, false},
		filterTest{And(AlwaysMatch, NeverMatch, AlwaysMatch), emptyCert, false},
		filterTest{And(NeverMatch, AlwaysMatch, NeverMatch), emptyCert, false},
		// OR
		filterTest{Or(AlwaysMatch), emptyCert, true},
		filterTest{Or(NeverMatch), emptyCert, false},
		filterTest{Or(AlwaysMatch, AlwaysMatch), emptyCert, true},
		filterTest{Or(NeverMatch, NeverMatch), emptyCert, false},
		filterTest{Or(AlwaysMatch, AlwaysMatch), emptyCert, true},
		filterTest{Or(NeverMatch, NeverMatch), emptyCert, false},
	}
)

func TestSimpleCombinations(t *testing.T) {
	for _, test := range simpleCombinationTests {
		r := test.filter(test.input)
		if r != test.expected {
			t.Error("For", test.filter, "expected", test.expected, "got", r)
		}
	}
}

type certType struct {
	filename string
	profiles []string
}

var (
	certTypeFilter = map[string]CertSelector{
		"User":                   FilterIsProfileUser, // 802.1X User
		"UserSign":               FilterIsProfileUserSign,
		"UserSignAndLogon":       FilterIsProfileUserSignAndLogon,
		"UserEncrypt":            FilterIsProfileUserEncryption,
		"CodeSigning":            FilterIsProfileCodeSigning,
		"RA-Operator":            FilterIsProfileRAOperator,
		"802.1X Client":          FilterIsProfileX8021XClient,
		"LDAP Server":            FilterIsProfileLDAPServer, // Mail, Radius, Shibb, VoIP
		"Web Server":             FilterIsProfileWebServer,  // VPN
		"Web Server Must Staple": FilterIsProfileWebServerMustStaple,
		"Domain Controller":      FilterIsProfileDomainController,
		"Exchange Server":        FilterIsProfileExchangeServer,
	}
	certTypeTests = []certType{
		certType{"testfiles/8021xclient.cert", []string{"802.1X Client"}},
		certType{"testfiles/codesigning.cert", []string{"CodeSigning"}},
		certType{"testfiles/domaincontroller.cert", []string{"Domain Controller"}},
		certType{"testfiles/exchangeserver.cert", []string{"Exchange Server"}},
		certType{"testfiles/ldapserver.cert", []string{"LDAP Server"}},
		certType{"testfiles/mailserver.cert", []string{"LDAP Server"}},
		certType{"testfiles/radiusserver.cert", []string{"LDAP Server"}},
		certType{"testfiles/shibbolethidpsp.cert", []string{"LDAP Server"}},
		certType{"testfiles/teilnehmerservice.cert", []string{"RA-Operator"}},
		certType{"testfiles/voipserver.cert", []string{"LDAP Server"}},
		certType{"testfiles/user.cert", []string{"User"}},
		certType{"testfiles/8021xuser.cert", []string{"User"}},
		certType{"testfiles/usersign.cert", []string{"UserSign"}},
		certType{"testfiles/usersignandlogon.cert", []string{"UserSignAndLogon"}},
		certType{"testfiles/userencrypt.cert", []string{"UserEncrypt"}},
		certType{"testfiles/vpnserver.cert", []string{"Web Server"}},
		certType{"testfiles/webserver.cert", []string{"Web Server"}},
		certType{"testfiles/webservermuststaple.cert", []string{"Web Server Must Staple"}},
	}
)

// test if FilterIsProfile* filters are correct and exclusive
func TestCertType(t *testing.T) {
	for _, testCert := range certTypeTests {
		// open certificate file
		cert := loadCertfile(testCert.filename)
		// O(1) access to certificate types
		typemap := make(map[string]bool)
		for _, t := range testCert.profiles {
			typemap[t] = true
		}
		// test both inclusion and exclusion for all known types
		for nr, filter := range certTypeFilter {
			if typemap[nr] == true {
				if filter(cert) == false {
					t.Error("Certificate", testCert.filename, "is not classified as type", nr, "but should.")
				}
			} else {
				if filter(cert) == true {
					t.Error("Certificate", testCert.filename, "is classified as type", nr, "but should not.")
				}
			}

		}
	}
}

type FileFilterDescription struct {
	filename    string
	filter      CertSelector
	description string
}

// Test prefix matches
func TestMoreDNFStuff(t *testing.T) {
	var tests = []FileFilterDescription{
		FileFilterDescription{
			"testfiles/test_group.cert",
			FilterIsGroup,
			"Gruppenzertifikat",
		},
		FileFilterDescription{
			"testfiles/test_pseudonym.cert",
			FilterIsPseudonym,
			"Pseudonymzertifikat",
		},
		FileFilterDescription{
			"testfiles/codesigning.cert",
			Not(FilterIsServer),
			"not a server certificate",
		},
		FileFilterDescription{
			"testfiles/user.cert",
			Not(FilterIsGroup),
			"not a group certificate",
		},
	}
	for _, test := range tests {
		c := loadCertfile(test.filename)
		if test.filter(c) != true {
			t.Error("Certificate", test.filename, "not classified as", test.description)
		}
	}
}
