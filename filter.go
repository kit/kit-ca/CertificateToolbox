package CertificateToolbox

import (
	"crypto/x509"
	"encoding/asn1"
	"net"
	"reflect"
	"regexp"
	"sort"
	"time"
)

type CertSelector func(*x509.Certificate) bool

var (
	ExtKeyUsageMicrosoftSmartcardLogon = asn1.ObjectIdentifier{1, 3, 6, 1, 4, 1, 311, 20, 2, 2}
	ExtKeyUsageKDCAuth                 = asn1.ObjectIdentifier{1, 3, 6, 1, 5, 2, 3, 5}
)

// Filter an array of certificates using a CertSelector function
func Filter(cs CertSelector, certs []*x509.Certificate) []*x509.Certificate {
	var filtered = make([]*x509.Certificate, 0)
	for _, c := range certs {
		if cs(c) {
			filtered = append(filtered, c)
		}
	}
	return filtered
}

// Divide a list of certificates into two lists: matching and non-matching
func Split(cs CertSelector, certs []*x509.Certificate) (match []*x509.Certificate, nonMatch []*x509.Certificate) {
	for _, c := range certs {
		if cs(c) {
			match = append(match, c)
		} else {
			nonMatch = append(nonMatch, c)
		}
	}
	return match, nonMatch
}

// true and false
func AlwaysMatch(*x509.Certificate) bool { return true }
func NeverMatch(*x509.Certificate) bool  { return false }

// invert selector (NOT)
func Not(cs CertSelector) CertSelector {
	return func(c *x509.Certificate) bool {
		return !cs(c)
	}
}

// combine multiple CertSelectors via logical AND
func And(cs ...CertSelector) CertSelector {
	return func(c *x509.Certificate) bool {
		for _, certSelector := range cs {
			if !certSelector(c) {
				return false
			}
		}
		return true
	}
}

// combine multiple CertSelectors via logical OR
func Or(cs ...CertSelector) CertSelector {
	return func(c *x509.Certificate) bool {
		for _, certSelector := range cs {
			if certSelector(c) {
				return true
			}
		}
		return false
	}
}

// filter by signing algorithm
func SignatureAlgorithmFilter(SignatureAlgorithm x509.SignatureAlgorithm) CertSelector {
	return func(c *x509.Certificate) bool {
		return c.SignatureAlgorithm == SignatureAlgorithm
	}
}

const (
	OnlyCN = 1 << iota
	Any    = 1 << iota
	All    = 1 << iota
)

// test if certificate names are found in DNS
func ValidDNSFilter(which int) CertSelector {
	return func(c *x509.Certificate) bool {
		switch which {
		case OnlyCN:
			_, err := net.LookupHost(c.Subject.CommonName)
			return err == nil
		case Any:
			for _, name := range c.DNSNames {
				_, err := net.LookupHost(name)
				if err == nil {
					return true
				}
			}
			return false
		case All:
			for _, name := range c.DNSNames {
				_, err := net.LookupHost(name)
				if err == nil {
					return false
				}
			}
			return true
		default:
			panic("Invalid parameter")
		}
	}
}

// test if certificate is valid at time t
func ValidAt(t time.Time) CertSelector {
	return func(c *x509.Certificate) bool {
		return t.After(c.NotBefore) && t.Before(c.NotAfter)
	}
}

// test if certificate's common name matches a regular expression
func CommonNameRegexpMatch(regex string) CertSelector {
	r := regexp.MustCompile(regex)
	return func(c *x509.Certificate) bool {
		return r.MatchString(c.Subject.CommonName)
	}
}

// build selector function for certain KeyUsage combinations
func KeyUsageFilter(usage ...x509.KeyUsage) CertSelector {
	var fullUsage int
	for _, bitmask := range usage {
		fullUsage |= int(bitmask)
	}
	return func(c *x509.Certificate) bool {
		return int(c.KeyUsage) == fullUsage
	}
}

// build selector function for certain extended key usages
func ExtKeyUsageFilter(usage ...x509.ExtKeyUsage) CertSelector {
	return func(c *x509.Certificate) bool {
		var haveExtKeyUsage, wantExtKeyUsage []int

		// shortcut
		if len(usage) != len(c.ExtKeyUsage) {
			return false
		}

		haveExtKeyUsage = make([]int, len(usage))
		wantExtKeyUsage = make([]int, len(c.ExtKeyUsage))

		// this is safe because we know that both array have the same size
		for i := 0; i < len(usage); i++ {
			haveExtKeyUsage[i] = int(usage[i])
			wantExtKeyUsage[i] = int(c.ExtKeyUsage[i])
		}

		sort.Ints(haveExtKeyUsage)
		sort.Ints(wantExtKeyUsage)

		return reflect.DeepEqual(haveExtKeyUsage, wantExtKeyUsage)
	}
}

// build selector function for extended key usages that Go does not know about
func UnknownExtKeyUsageFilter(usage ...asn1.ObjectIdentifier) CertSelector {
	return func(c *x509.Certificate) bool {
		var haveUnknownExtKeyUsage, wantUnknownExtKeyUsage []string

		// shortcut
		if len(usage) != len(c.UnknownExtKeyUsage) {
			return false
		}

		haveUnknownExtKeyUsage = make([]string, len(usage))
		wantUnknownExtKeyUsage = make([]string, len(c.UnknownExtKeyUsage))

		for i := 0; i < len(usage); i++ {
			haveUnknownExtKeyUsage[i] = usage[i].String()
			wantUnknownExtKeyUsage[i] = c.UnknownExtKeyUsage[i].String()
		}

		sort.Strings(haveUnknownExtKeyUsage)
		sort.Strings(wantUnknownExtKeyUsage)

		return reflect.DeepEqual(haveUnknownExtKeyUsage, wantUnknownExtKeyUsage)
	}
}

// Build selector function for certain certificate extensions.
// This filter only checks if the extension is present, value and critical-flag are ignored
func ExtensionPresentFilter(extension ...asn1.ObjectIdentifier) CertSelector {
	return func(c *x509.Certificate) bool {
		var haveExtensions = make(map[string]bool)
		for _, haveExtension := range c.Extensions {
			haveExtensions[haveExtension.Id.String()] = true
		}
		for _, wantId := range extension {
			_, ok := haveExtensions[wantId.String()]
			if !ok {
				return false
			}
		}
		return true
	}
}
