package CertificateToolbox

import (
	"crypto/x509"
	"strings"
	"time"
)

type ExceptionMaps struct {
	Serial map[string]bool
	Email  map[string]bool
}

// convert list of strings to map[string]bool for O(1) inclusion tests
func listToMap(in []string) map[string]bool {
	var (
		out = make(map[string]bool)
	)
	for _, key := range in {
		out[strings.ToLower(key)] = true
	}
	return out
}

// NewExceptionMaps creates new a new ExceptionMaps from arrays of serial numbers & mail addresses
func NewExceptionMaps(serial, mail []string) ExceptionMaps {
	return ExceptionMaps{
		Serial: listToMap(serial),
		Email:  listToMap(mail),
	}
}

func (lists *ExceptionMaps) MatchExceptions(cert *x509.Certificate) bool {
	serial10 := cert.SerialNumber.Text(10)
	serial16 := cert.SerialNumber.Text(16)
	serial0x16 := "0x" + serial16
	// check serial numbers
	for _, s := range []string{serial10, serial16, serial0x16} {
		if _, present := lists.Serial[s]; present {
			return true
		}
	}
	// check email whitelists
	for _, mailaddress := range cert.EmailAddresses {
		if _, present := lists.Email[strings.ToLower(mailaddress)]; present {
			return true
		}
	}
	return false
}

// CreatePublicationFilter creates a filter with some very opinionated views on what should be published to the Active Directory
func CreatePublicationFilter(whitelist, blacklist ExceptionMaps) func(cert *x509.Certificate) bool {
	return func(cert *x509.Certificate) bool {
		// check serial blacklists
		if blacklist.MatchExceptions(cert) {
			return false
		}
		var now = time.Now()
		// skip expired certificate
		if now.After(cert.NotAfter) {
			return false
		}
		// skip certificates with no email addresses
		if len(cert.EmailAddresses) <= 0 {
			return false
		}
		// skip certificates without EmailProtection in ExtendedKeyUsage
		{
			foundEmailProtection := false
			for _, extension := range cert.ExtKeyUsage {
				if extension == x509.ExtKeyUsageEmailProtection {
					foundEmailProtection = true
				}
			}
			if !foundEmailProtection {
				return false
			}
		}
		// skip certificates without Key Encipherment in KeyUsage
		if cert.KeyUsage&x509.KeyUsageKeyEncipherment == 0 {
			return false
		}
		// check whitelists
		if whitelist.MatchExceptions(cert) {
			return true
		}
		// skip PN and EXT with certain words (API|Teilnehmerservice|Login|Sign|Test|Demo|Apple)
		if MatchSubjectPNEXT.MatchString(cert.Subject.CommonName) &&
			MatchSubjectNoMail.MatchString(cert.Subject.CommonName) {
			return false
		}
		// remove RA-Ops (new)
		if strings.Contains(cert.Subject.CommonName, "Teilnehmerservice") {
			return false
		}
		// remove RA-Ops (old)
		if strings.Contains(cert.Subject.CommonName, "RA-Operator") {
			return false
		}
		// don't publish CA certificates
		if cert.IsCA {
			return false
		}
		return true
	}
}
