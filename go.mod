module gitlab.kit.edu/kit/kit-ca/CertificateToolbox

go 1.17

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/pkg/errors v0.9.1
	golang.org/x/crypto v0.0.0-20220131195533-30dcbda58838
)

require golang.org/x/sys v0.0.0-20210615035016-665e8c7367d1 // indirect
